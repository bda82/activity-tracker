from datetime import datetime

from settings import Settings


def get_now_format(fmt: str | None = None) -> str:
    if fmt:
        return datetime.now().strftime(fmt)
    else:
        return datetime.now().strftime(Settings().dt_format)


def milliseconds_to_time(ms: int):
    ms = ms // 1000
    time = ms % (24 * 3600)
    hours = time // 3600
    time %= 3600
    minutes = time // 60
    time %= 60
    seconds = time
    return {
        'ms': ms,
        'seconds': seconds,
        'minutes': minutes,
        'hours': hours,
    }
