from typing import Any

import requests as requests

from settings import Settings


class Client:
    settings = Settings()
    methods = {
        'get': requests.get,
        'post': requests.post
    }

    def __fetch(
        self,
        url: str,
        method: str = 'get',
        data: dict | None = None,
        headers: dict | None = None,
        files: Any | None = None
    ):
        if data is None:
            data = {}
        try:
            if data and not files:
                r = self.methods[method](url, json=data, headers=headers)
            elif not data and files:
                r = self.methods[method](url, files=files, headers=headers)
            else:
                r = self.methods[method](url, headers=headers)
            if r.status_code == 200:
                return r.json()
            return None
        except Exception as e:
            print(e)

    def auth(self, email, password):
        data = {
            'email': email,
            'password': password
        }
        headers = {
            'Content-Type': 'application/json'
        }
        url = f'{self.settings.server_url}{self.settings.urls.auth}'
        return self.__fetch(url=url, method='post', data=data, headers=headers)

    def get_projects(self, auth_token: str):
        data = None
        headers = {
            'Content-Type': 'application/json',
            'Authorization': auth_token
        }
        url = f'{self.settings.server_url}{self.settings.urls.get_projects}'
        return self.__fetch(url=url, method='get', data=data, headers=headers)

    def project_toggle(self, auth_token: str, project_id: int):
        data = None
        headers = {
            'Content-Type': 'application/json',
            'Authorization': auth_token
        }
        url = f'{self.settings.server_url}{self.settings.urls.project_toggle}?projectId={project_id}'
        return self.__fetch(url=url, method='get', data=data, headers=headers)

    def work_activity(self, auth_token):
        data = None
        headers = {
            'Content-Type': 'application/json',
            'Authorization': auth_token
        }
        url = f'{self.settings.server_url}{self.settings.urls.work_activity}'
        return self.__fetch(url=url, method='get', data=data, headers=headers)

    def send_screenshot(self, auth_token, screen_path: str, project_id: int):
        headers = {
            'Authorization': auth_token
        }
        files = {'media': open(screen_path, 'rb')}
        url = f'{self.settings.server_url}{self.settings.urls.send_screenshot}?projectId={project_id}'
        return self.__fetch(url=url, method='post', files=files, headers=headers)

    def send_intensity(self, auth_token, intensity: int, project_id: int):
        data = {
            'intensity': intensity
        }
        headers = {
            'Authorization': auth_token
        }
        url = f'{self.settings.server_url}{self.settings.urls.intensity}?projectId={project_id}'
        return self.__fetch(url=url, method='post', data=data, headers=headers)

    def send_processes(self, auth_token, processes: list, project_id: int):
        data = {
            'processes': processes
        }
        headers = {
            'Authorization': auth_token
        }
        url = f'{self.settings.server_url}{self.settings.urls.send_processes}?projectId={project_id}'
        return self.__fetch(url=url, method='post', data=data, headers=headers)