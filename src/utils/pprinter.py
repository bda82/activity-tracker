from pprint import PrettyPrinter
from typing import Any

pp = PrettyPrinter()


def pprint(data: Any, message: str | None):
    if message:
        print(f'{message}')
    pp.pprint(data)
