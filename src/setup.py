import os
from distutils.core import  setup


def __read__(file_name):
    return open(os.path.join(os.path.dirname(__file__), file_name)).read()


setup(
    name='WebSailors Tracker',
    version='1.0.1',
    description='WebSailors Tracker Client Application',
    author_email='dmitry.bespalov@websailors.pro',
    author='dmitry.bespalov',
    url='https://websailors.pro',
    packages=[
        'tracker',
    ],
    long_description=__read__('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "Environment :: X11 Applications",
        "Intended Audience :: System Administrators"
        "License :: OSI Approved :: Apache Software License"
    ],
    setup_requires=[
        'aiosignal==1.2.0',
        'attrs==21.4.0',
        'certifi==2022.6.15',
        'cffi==1.15.1',
        'charset-normalizer==2.1.0',
        'cryptography==37.0.4',
        'evdev==1.6.0',
        'frozenlist==1.3.0',
        'idna==3.3',
        'Jinja2==3.1.2',
        'MarkupSafe==2.1.1',
        'multidict==6.0.2',
        'Pillow==9.2.0',
        'psutil==5.9.1',
        'pycparser==2.21',
        'pynput==1.7.6',
        'PySide6==6.3.1',
        'PySide6-Addons==6.3.1',
        'PySide6-Essentials==6.3.1',
        'python-xlib==0.31',
        'qt-material==2.12',
        'requests==2.28.1',
        'shiboken6==6.3.1',
        'six==1.16.0',
        'urllib3==1.26.11',
        'yarl==1.7.2',
    ],
    install_requires=[
        'aiosignal==1.2.0',
        'attrs==21.4.0',
        'certifi==2022.6.15',
        'cffi==1.15.1',
        'charset-normalizer==2.1.0',
        'cryptography==37.0.4',
        'evdev==1.6.0',
        'frozenlist==1.3.0',
        'idna==3.3',
        'Jinja2==3.1.2',
        'MarkupSafe==2.1.1',
        'multidict==6.0.2',
        'Pillow==9.2.0',
        'psutil==5.9.1',
        'pycparser==2.21',
        'pynput==1.7.6',
        'PySide6==6.3.1',
        'PySide6-Addons==6.3.1',
        'PySide6-Essentials==6.3.1',
        'python-xlib==0.31',
        'qt-material==2.12',
        'requests==2.28.1',
        'shiboken6==6.3.1',
        'six==1.16.0',
        'urllib3==1.26.11',
        'yarl==1.7.2',
    ],
    scripts=[
        'src/tracker.py'
    ]
)