import json
import os
from styles import Styles
from cryptography.fernet import Fernet


class Urls:
    auth = 'auth/login'
    get_projects = 'projects/with_record'
    project_toggle = 'tracker/toggle'
    work_activity = 'records/work_activity'
    send_screenshot = 'tracker/screenshot'
    intensity = 'tracker/intensity'
    send_processes = 'tracker/processes'


class Settings:

    def __init__(self):
        self.secret_key = b'1QkaK9nLX1ON1ymnBZJdutbFdzxS1P74WQ6WuFB_o7E='
        self.window_height = 500
        self.window_width = 300
        self.theme = 'light_cyan_500.xml'
        self.settings_filename = 'wstracker.json'
        self.window_title = 'WebSailors Tracker Client'
        self.home_folder = '~/.tracker'
        self.screen_file_prefix = 'scr_'
        self.screen_file_extension = '.png'
        self.dt_format = "%Y_%m_%d-%I:%M:%S_%p"
        self.screen_capture_interval = 5 * 60
        self.intensity_measurement_interval = 60
        self.intensity_measurement_maximum = 60
        self.session_timer_session_length = 1
        self.num_of_attempts = 3
        self.request_timeout = 5
        self.sleep_time = 10
        self.server_url = 'https://time.websailors.pro/api/'
        self.login = ''
        self.password = ''
        self.urls = Urls()
        self.styles = Styles()

        self.load_from_file()

        self.mix_settings()

    def mix_settings(self) -> None:
        settings = self.load_from_file()
        self.login = settings['login']
        self.password = settings['password']

    def prepare(self):
        home = os.path.expanduser(self.home_folder)

        if not os.path.exists(home):
            os.mkdir(home)

        file_path = home + '/' + self.settings_filename

        data = {
            'theme': self.theme,
            'home_folder': self.home_folder,
            'login': self.login,
            'password': self.password
        }

        return file_path, data

    def load_from_file(self) -> dict:
        file_path, data = self.prepare()
        fernet = Fernet(self.secret_key)

        if not os.path.exists(file_path):
            with open(file_path, 'w') as f:
                text = json.dumps(data)
                encrypted = fernet.encrypt(str.encode(text))
                f.write(encrypted.decode())
        else:
            with open(file_path, 'r') as f:
                text = f.read()
                encrypted = fernet.decrypt(str.encode(text))
                data = json.loads(encrypted.decode())

        return data

    def save_to_file(self) -> dict:
        file_path, data = self.prepare()
        fernet = Fernet(self.secret_key)

        with open(file_path, 'w+') as f:
            text = json.dumps(data)
            encrypted = fernet.encrypt(str.encode(text))
            f.write(encrypted.decode())

        return data
