import os
import psutil
from enum import Enum

from PIL import ImageGrab
from PySide6.QtCore import QTimer
from PySide6.QtGui import QPixmap, QFont
from PySide6.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QMessageBox
from pynput import keyboard

from utils.client import Client

from settings import Settings
from utils.dt import get_now_format, milliseconds_to_time
from windows.components.line import get_line
from windows.components.progress import get_progress
from windows.login_dialog import LoginDialog
from windows.view_screenshot_dialog import ViewScreenshotDialog

from windows.components.label import get_label
from windows.components.selector import get_selector
from windows.components.button import get_button


class State(Enum):
    WAIT_FOR_LOGIN = 'WAIT_FOR_LOGIN'
    WAIT_FOR_START = 'WAIT_FOR_START'
    STOP = 'STOP'
    WORK = 'WORK'
    START = 'START'


class MainWindow(QWidget):

    def __init__(self) -> None:
        super(MainWindow, self).__init__()

        self.settings = Settings()

        self.client = Client()

        self.path = os.path.expanduser(self.settings.home_folder)

        self.project_toggle = False
        self.project_toggle_time = None
        self.projects = []
        self.current_project = None
        self.project_milliseconds = 0
        self.intensity = 0

        self.keyboard_listener = None

        self.last_screenshot = None
        self.last_screenshot_path = None

        self.session_timer = QTimer()
        self.intensity_timer = QTimer()
        self.session_timer_session_length = self.settings.session_timer_session_length
        self.intensity_measurement_interval = self.settings.intensity_measurement_interval
        self.intensity_measurement_maximum = self.settings.intensity_measurement_maximum

        self.screen_capture_counter = 0
        self.screen_captire_interval = self.settings.screen_capture_interval

        self.state = State.WAIT_FOR_LOGIN.value

        self.__auth_token = None

        self.__init_tracker_folder()
        self.__init_ui()

        self.session_timer.timeout.connect(self.session_timer_tick)
        self.intensity_timer.timeout.connect(self.intensity_timer_tick)
        self.process_state()

    def __init_tracker_folder(self):
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        for path in os.listdir(self.path):
            if os.path.isfile(os.path.join(self.path, path)):
                if path.endswith(self.settings.screen_file_extension):
                    os.remove(os.path.join(self.path, path))

    def __init_ui(self):
        self.setWindowTitle(self.settings.window_title)

        # Layouts

        self.main_layout = QVBoxLayout()
        self.time_layout = QVBoxLayout()
        self.intensity_layout = QVBoxLayout()
        self.project_layout = QVBoxLayout()
        self.screen_view_layout = QVBoxLayout()
        self.control_layout = QHBoxLayout()

        # Widgets

        # > Time
        self.time_title = get_label('My Time:', style='font-size: 18px;')
        self.time_counter = get_label(
            text='00:00:00',
            style='border: 1px solid black; font-size: 24px;',
            width=self.settings.window_width - 10,
            height=50
        )
        # > Intensity
        self.intensity_title = get_label('Intensity:', style='font-size: 18px;')
        self.intensity_progress = get_progress(0, self.settings.intensity_measurement_maximum)
        # > Project
        self.project_title_label = get_label(text='Select Project:', style='font-size: 18px;')
        self.project_selector = get_selector()
        self.project_selector.activated.connect(self.on_project_selector_changed)
        # > Screens
        self.last_screen_label = get_label(text='Last Screenshot:', style='font-size: 18px;')
        self.image_view = get_label()
        self.image_view.setFixedSize(290, 120)
        self.image_view.setScaledContents(True)
        self.image_view.mousePressEvent = self.on_image_view_click
        # > Control
        self.control_start_button = get_button('START', self.on_start_button_click)
        self.control_stop_button = get_button('STOP', self.on_stop_button_click)
        self.control_login_button = get_button('LOGIN', self.on_login_button_click)

        # Layouts

        # > Time
        self.time_layout.addWidget(self.time_title)
        self.time_layout.addWidget(self.time_counter)
        self.time_layout.setSpacing(10)
        # > Intensity
        self.intensity_layout.addWidget(self.intensity_title)
        self.intensity_layout.addWidget(self.intensity_progress)
        # > Project
        self.project_layout.addWidget(self.project_title_label)
        self.project_layout.addWidget(self.project_selector)
        self.project_layout.addWidget(get_line())
        self.project_layout.setSpacing(10)
        # > Screens
        self.screen_view_layout.addWidget(self.last_screen_label)
        self.screen_view_layout.addWidget(self.image_view)
        # > Control
        self.control_layout.addWidget(self.control_start_button)
        self.control_layout.addWidget(self.control_stop_button)
        self.control_layout.addWidget(self.control_login_button)

        # Window fill

        self.main_layout.addLayout(self.time_layout)
        self.main_layout.addLayout(self.intensity_layout)
        self.main_layout.addLayout(self.project_layout)
        self.main_layout.addLayout(self.screen_view_layout)
        self.main_layout.addLayout(self.control_layout)

        self.setLayout(self.main_layout)

    def process_state(self):
        if self.state == State.WAIT_FOR_LOGIN.value:
            self.control_start_button.setEnabled(False)
            self.control_stop_button.setEnabled(False)

        if self.state == State.WAIT_FOR_START.value:
            self.control_start_button.setEnabled(True)
            self.control_stop_button.setEnabled(False)
            self.control_login_button.setEnabled(False)
            r = self.client.get_projects(self.__auth_token)
            if r:
                self.projects = r.get('result', [])
            else:
                self.projects = []
            self.fill_projects_selector()
            if not self.projects:
                self.current_project = None
                self.project_toggle = False
                self.control_start_button.setEnabled(False)
                self.control_stop_button.setEnabled(False)
                self.control_login_button.setEnabled(False)
                QMessageBox.about(
                    self,
                    'No Project',
                    'You don`t have any active projects, or you are not logged in. Please, contact the Administrator!'
                )
                return
            if self.projects and not self.current_project:
                self.current_project = self.projects[0]
            for project in self.projects:
                record = project.get('record')
                pid = project.get('id')
                enabled = record.get('enable')
                if enabled:
                    self.client.project_toggle(self.__auth_token, pid)
                    self.project_toggle = False
                    QMessageBox.about(
                        self,
                        'Toggle Project',
                        'You had Started Project on Server - so we Stop it. Please, Start it in application again!'
                    )
            record = self.current_project.get('record', {})
            if record is not None:
                self.project_milliseconds = record.get('milliseconds', 0)
            else:
                self.project_milliseconds = 0
            self.recalculate_time_and_set_text(self.project_milliseconds)

        if self.state == State.START.value:
            self.project_selector.setEnabled(False)
            self.screen_capture_counter = 0
            self.session_timer.start(self.session_timer_session_length * 1000)
            self.intensity_timer.start(self.intensity_measurement_interval * 1000)
            if self.current_project:
                record = self.current_project.get('record', {})
                if record is not None:
                    self.project_milliseconds = record.get('milliseconds', 0)
                else:
                    self.project_milliseconds = 0
                self.recalculate_time_and_set_text(self.project_milliseconds)
                self.client.project_toggle(self.__auth_token, self.current_project['id'])
                self.project_toggle = True
                self.control_start_button.setEnabled(False)
                self.control_stop_button.setEnabled(True)
                self.state = State.WORK.value
                if self.keyboard_listener is None:
                    self.start_keyboard_listener()
                self.make_screenshot_send_and_show()

        if self.state == State.WORK.value:
            pass

        if self.state == State.STOP.value:
            self.project_selector.setEnabled(True)
            self.session_timer.stop()
            if self.current_project:
                self.client.project_toggle(self.__auth_token, self.current_project['id'])
                self.project_toggle = False
            self.control_start_button.setEnabled(True)
            self.control_stop_button.setEnabled(False)
            self.time_counter.setText('00:00:00')
            self.state = State.WAIT_FOR_START.value
            if self.keyboard_listener is not None:
                self.stop_keyboard_listener()
                self.keyboard_listener = None
                self.intensity_timer.stop()
            self.intensity_progress.setValue(0)
            self.keyboard_key_count = 0

    def recalculate_time_and_set_text(self, ms):
        dt = milliseconds_to_time(ms)
        hours = dt.get('hours', 0)
        if hours < 10:
            hours = f'0{hours}'
        minutes = dt.get('minutes', 0)
        if minutes < 10:
            minutes = f'0{minutes}'
        seconds = dt.get('seconds', 0)
        if seconds < 10:
            seconds = f'0{seconds}'
        self.time_counter.setText(
            f'{hours}:{minutes}:{seconds}'
        )

    def fill_projects_selector(self):
        projects = [p.get('name', '') for p in self.projects]
        index = self.project_selector.currentIndex()
        if index == -1:
            index = 0
        self.project_selector.clear()
        self.project_selector.addItems(projects)
        self.project_selector.setCurrentIndex(index)

    def start_keyboard_listener(self):
        if self.keyboard_listener is None:
            self.keyboard_listener = keyboard.Listener(
                on_press=self.on_system_keyboard_press,
                on_release=self.on_system_keyboard_press_release)
            self.keyboard_listener.start()
            self.keyboard_key_count = 0

    def stop_keyboard_listener(self):
        if self.keyboard_listener is not None:
            self.keyboard_listener.stop()
            self.keyboard_listener = None
            self.keyboard_key_count = 0

    def intensity_timer_tick(self):
        if self.keyboard_key_count < self.intensity_measurement_maximum:
            self.intensity_progress.setValue(self.keyboard_key_count)
        else:
            self.intensity_progress.setValue(self.intensity_measurement_maximum - 1)
        self.client.send_intensity(self.__auth_token, self.keyboard_key_count, self.current_project['id'])
        processes = self.get_processes_list()
        self.client.send_processes(self.__auth_token, processes, self.current_project['id'])
        self.keyboard_key_count = 0

    def on_system_keyboard_press(self, key):
        if self.keyboard_listener is not None:
            self.keyboard_key_count += 1

    def on_system_keyboard_press_release(self, key):
        pass

    @staticmethod
    def get_processes_list():
        processes = []
        for proc in psutil.process_iter():
            try:
                pinfo = proc.as_dict(attrs=['pid', 'name', 'cpu_percent'])
                pinfo['vms'] = proc.memory_info().vms / (1024 * 1024)
                processes.append(pinfo)
            except Exception as e:
                print(e)
        processes = sorted(processes, key=lambda proc_obj: proc_obj['vms'], reverse=True)
        return processes

    def on_project_selector_changed(self, value):
        self.current_project = self.projects[value]
        self.project_toggle = False
        self.state = State.WAIT_FOR_START.value
        self.process_state()

    def on_start_button_click(self):
        self.state = State.START.value
        self.process_state()

    def on_stop_button_click(self):
        self.state = State.STOP.value
        self.process_state()

    def on_login_button_click(self):
        view = LoginDialog(self, self.settings)
        view.exec_()
        self.__auth_token = view.auth_token
        self.state = State.WAIT_FOR_START.value
        self.process_state()

    def on_image_view_click(self, event):
        if self.last_screenshot_path is not None:
            view = ViewScreenshotDialog(self, self.last_screenshot_path)

    def take_screenshot(self):
        self.last_screenshot = ImageGrab.grab()
        self.last_screenshot = self.last_screenshot.convert('LA')
        return self.last_screenshot

    def save_screenshot(self, screenshot):
        path = self.settings.home_folder
        path = os.path.expanduser(path)
        filename = self.settings.screen_file_prefix + get_now_format() + self.settings.screen_file_extension
        fullpath = os.path.join(path, filename)
        screenshot.save(fullpath, quality=20, optimize=True)
        screenshot.close()
        return fullpath, filename

    def make_screenshot_send_and_show(self):
        screenshot = self.take_screenshot()
        fullpath, filename = self.save_screenshot(screenshot)
        self.last_screenshot_path = fullpath
        picture = QPixmap(fullpath)
        self.image_view.setPixmap(picture)
        self.image_view.adjustSize()
        self.image_view.show()
        self.client.send_screenshot(self.__auth_token, fullpath, self.current_project['id'])

    def session_timer_tick(self):
        self.project_milliseconds += self.session_timer_session_length * 1000
        self.recalculate_time_and_set_text(self.project_milliseconds)
        if self.screen_capture_counter > self.screen_captire_interval:
            self.screen_capture_counter = 0
            self.make_screenshot_send_and_show()
        self.screen_capture_counter += 1

    def closeEvent(self, event):
        if self.current_project and self.project_toggle:
            msg_box = QMessageBox()
            msg_box.setIcon(QMessageBox.Information)
            msg_box.setWindowTitle('WebSailors Tracker')
            msg_box.setText('Do you really want to close the app? In this case, the Tracker will be stopped!')
            msg_box.setDetailedText(f'Now you work with Project: \n   {self.current_project["name"]}')
            msg_box.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)

            return_value = msg_box.exec()

            if return_value == QMessageBox.Ok:
                self.client.project_toggle(self.__auth_token, self.current_project['id'])
                event.accept()
            else:
                event.ignore()
