from PySide6.QtWidgets import QDialog, QVBoxLayout, QMessageBox

from utils.client import Client
from windows.components.button import get_button
from windows.components.line_edit import get_line_edit
from windows.components.label import get_label
from windows.components.line import get_line


class LoginDialog(QDialog):

    def __init__(self, parent, settings):
        super(LoginDialog, self).__init__(parent)
        self.settings = settings
        self.auth_token = None

        self.client = Client()

        self.setWindowTitle('Login')

        self.main_layout = QVBoxLayout()

        self.label_login = get_label('Email:')
        self.label_password = get_label('Password:')
        self.edit_login = get_line_edit(self.settings.login)
        self.edit_password = get_line_edit(self.settings.password)
        self.button_login = get_button('LOGIN', self.on_button_login_clicked)

        self.button_apply = get_button('CLOSE', self.on_button_apply_clicked)

        self.main_layout.addWidget(self.label_login)
        self.main_layout.addWidget(self.edit_login)
        self.main_layout.addWidget(self.label_password)
        self.main_layout.addWidget(self.edit_password)
        self.main_layout.addWidget(self.button_login)
        self.main_layout.addWidget(get_line())
        self.main_layout.addWidget(get_line())
        self.main_layout.addWidget(self.button_apply)

        self.main_layout.addStretch(1)
        self.setLayout(self.main_layout)

    def on_button_login_clicked(self):
        email = self.edit_login.text()
        password = self.edit_password.text()
        r = self.client.auth(email, password)
        if r and r.get('message') == 'Success':
            self.auth_token = r.get('result', {}).get('token', None)
            QMessageBox.about(self, 'Login', 'You are logged in successfully!')
        else:
            self.auth_token = None
            QMessageBox.about(self, 'Login', 'Login Error!')

    def on_button_apply_clicked(self):
        self.settings.login = self.edit_login.text()
        self.settings.password = self.edit_password.text()
        self.settings.save_to_file()
        self.close()
