from PySide6.QtGui import QPixmap
from PySide6.QtWidgets import QLabel, QDialog


class ViewScreenshotDialog(QDialog):
    def __init__(self, parent, fullpath):
        super(ViewScreenshotDialog, self).__init__(parent)
        self.image_view = QLabel()
        self.image_view.setScaledContents(True)
        picture = QPixmap(fullpath)
        self.image_view.setPixmap(picture)
        self.image_view.adjustSize()
        self.image_view.show()
