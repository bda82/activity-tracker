from PySide6.QtWidgets import QLineEdit


def get_line_edit(text: str = ''):
    return QLineEdit(text)
