from typing import Any, Optional

from PySide6.QtWidgets import QLabel


def get_label(text: str = '', style: Any = None, width: int | None = None, height: int | None = None):
    label = QLabel(text)
    if style:
        label.setStyleSheet(style)
    if width and height:
        label.setFixedSize(width, height)
    return label
