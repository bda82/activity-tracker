from typing import Any

from PySide6.QtWidgets import QPushButton


def get_button(title: str, callback: Any, icon: Any = None):
    button = QPushButton(title)
    button.clicked.connect(callback)
    return button
