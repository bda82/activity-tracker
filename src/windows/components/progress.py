from PySide6.QtWidgets import QProgressBar


def get_progress(start: int = 0, limit: int = 100):
    p = QProgressBar()
    p.setRange(start, limit)
    return p
