from PySide6.QtWidgets import QTextEdit


def get_text_edit(text: str = ''):
    return QTextEdit(text)
