from windows.components.label import get_label

from settings import Settings


def get_line():
    line = get_label('')
    line.setStyleSheet(Settings().styles.line_style)
    line.setMaximumHeight(2)
    return line
