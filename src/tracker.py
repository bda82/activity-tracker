import os
import sys

from PySide6.QtGui import QIcon, QAction
from PySide6.QtWidgets import QApplication, QSystemTrayIcon, QMenu
from qt_material import apply_stylesheet

from settings import Settings

from windows.main_window import MainWindow

basedir = os.path.dirname(__file__)


def main():
    app = QApplication(sys.argv)
    icon = QIcon(os.path.join(basedir, 'icons', 'time.png'))

    window = MainWindow()
    window.setMaximumHeight(Settings().window_height)
    window.setMaximumWidth(Settings().window_width)
    window.setWindowIcon(icon)
    window.show()

    apply_stylesheet(app, theme=Settings().theme)

    tray = QSystemTrayIcon()
    tray.setIcon(icon)
    tray.setVisible(True)

    menu = QMenu()
    option_hide = QAction("Hide")
    option_show = QAction("Show")
    option_exit = QAction("Quit")

    option_exit.triggered.connect(app.quit)
    option_show.triggered.connect(window.show)
    option_hide.triggered.connect(window.hide)

    menu.addAction(option_show)
    menu.addAction(option_hide)
    menu.addAction(option_exit)

    tray.setContextMenu(menu)

    app.exec()


if __name__ == '__main__':
    sys.exit(main())
