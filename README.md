# WebSailors Activity Tracker Client

### Application flow

- application work with WebSailors server API, so you need email/password for WebSailors Tracker web version
- run application
- click Login button
- enter email/password into according fields
- click Login button
- if the login process was successful, you will see the success message and click Close button
- choose Project in 'Select Project' box
- click on Start button

### Application abilities

- application has 'tray' icon: you can Show, Hide and Quit application
- application take screenshots from your displays: you can see the last one via click in 'Last Screenshot' area
- application take your running processes (you can not see this information in app.)
- application measure your activity: you can see this in progressbar
- application save your email/password in home folder, but encrypt data: you may be sure in security of this information
- application check if one of your Projects started on server (for example via web version), note you and stop tracker
- application prevent closing if one of project started in app., note you and show project details

### Build application on your platform

- create Python Virtualenv as ```venv``` folder like this ```virtualenv -p /usr/bin/python3.10 venv```
- activate env. like this: ```source venv/bin/activate```
- install packages like this: ```pip install -r requirements.txt```
- run ```build.sh``` file
- discover ```src/dist``` folder for executable ```tracker``` file

### Application Notes

- application built with Python3.10
- application in testing now
- application platforms: Linux (tested), MacOS (not tested), Windows (not tested)

### Gallery

#### Start Screen

![Start Screen](docs/assets/start_screen.png)

#### Login

![Login](docs/assets/login.png)

#### Success Login 
![Success Login](docs/assets/success_login.png)

#### Wait for start
![Wait for start](docs/assets/wait_for_start.png)

#### Start Projects
![Start Projects](docs/assets/start_project.png)

#### Intensity measurement
![Intensity](docs/assets/intensity.png)

#### Close started project
![Close started project](docs/assets/close_started_project.png)

### &copy; 2022, WebSailors