#!/bin/bash

if [ -d "./venv" ]
then
  source venv/bin/activate
else
  echo "There are no 'venv' folder with python virtual environment"
  exit
fi

if [ -d "./src" ]
then
  cd src
else
  echo "'src' folder does not exists"
  exit
fi

python3 -m PyInstaller --noconsole --windowed --onefile --add-data="icons/time.png:icons" tracker.py
